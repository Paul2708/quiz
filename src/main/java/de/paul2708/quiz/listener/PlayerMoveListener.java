package de.paul2708.quiz.listener;

import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.game.Voting;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Paul on 18.06.2016.
 */
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (Game.getRound() == null) return;

        if (Voting.getMap() != null) {
            if (e.getFrom().getY() <= Voting.getMap().getTeleportY()) {
                e.getPlayer().teleport(Voting.getMap().getSpawn());
            }
        }

        if (Game.getRound().canMove()) return;

        Location from = e.getFrom();
        Location to = e.getTo();
        double x = Math.floor(from.getX());
        double z = Math.floor(from.getZ());

        if (Math.floor(to.getX()) !=x || Math.floor(to.getZ()) !=z){
            x+=.5;
            z+=.5;
            Location newLoc = new Location(from.getWorld(),x,from.getY(),z,from.getYaw(),from.getPitch());
            e.getPlayer().teleport(newLoc);
        }
    }
}
