package de.paul2708.quiz.listener;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.util.Permission;
import de.paul2708.spigot.api.PlayerAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Paul on 10.07.2016.
 */
public class PlayerCommandPreprocessListener implements Listener {

    // TODO: SChauen ob wegen cloud cmds

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();
        String message = e.getMessage().trim();

        if (p.isOp() || PlayerAPI.getRank(p).getPermissionLevel() >= Permission.ALL_COMMANDS) return;

        if (Quiz.getConfigFile().isInSetup()) return;

        if (message.equalsIgnoreCase("start") || message.contains("stats") || message.contains("statistic") || message.contains("statistik")) return;

        e.setCancelled(true);

    }
}
