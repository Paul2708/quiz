package de.paul2708.quiz.listener.game;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 18.06.2016.
 */
public class GameInteractEntityListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();

        if (ServerAPI.getState() != GameState.INGAME) return;

        if (e.getRightClicked() instanceof Player) {
            if (p.getItemInHand() != null && p.getItemInHand().getType() == Material.LEASH) {
                if (Game.getRound() == null || !Game.getRound().canUseItems()) return;
                Location temp = p.getLocation();
                p.teleport(e.getRightClicked());
                e.getRightClicked().teleport(temp);
                p.sendMessage(Constants.TAG + "§7Du hast deine Position mit §e" + e.getRightClicked().getName() + " §7getauscht.");
                e.getRightClicked().sendMessage(Constants.TAG + "§7Du wurdest mit §e" + PlayerAPI.getRank(p).getPrefix() + p.getName() + " §7getauscht.");

                p.setItemInHand(new ItemStack(Material.AIR));
            }
        }
    }
}
