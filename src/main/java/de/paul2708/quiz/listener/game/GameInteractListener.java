package de.paul2708.quiz.listener.game;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.game.Profile;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 18.06.2016.
 */
public class GameInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Action action = e.getAction();
        ItemStack item = e.getItem();

        if(item == null || item.getType() == null) return;
        if(item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null) return;
        if(item.getType() == Material.AIR) return;
        if(ServerAPI.getState() != GameState.INGAME) return;

        e.setCancelled(true);

        String name = ChatColor.stripColor(item.getItemMeta().getDisplayName());

        if(action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
            if (Game.getRound() == null || !Game.getRound().canUseItems()) return;
            // Joker
            if (item.getType() == Material.NETHER_STAR) {
                Profile.getProfile(p).joker(true);
                p.sendMessage(Constants.TAG + "§eDu bekommst für diese keine Runde weder Punkte dazu noch abgezogen.");
                p.setItemInHand(new ItemStack(Material.AIR));
            }
            // Visible
            if (item.getType() == Material.BLAZE_ROD) {
                Bukkit.getOnlinePlayers().forEach(p::showPlayer);

                p.sendMessage(Constants.TAG + "§eDu siehst nun deine Mitspieler.");
                p.setItemInHand(new ItemStack(Material.AIR));
            }
        }
    }
}
