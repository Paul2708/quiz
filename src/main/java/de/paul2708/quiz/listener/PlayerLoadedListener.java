package de.paul2708.quiz.listener;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.stats.Stats;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.ItemManager;
import de.paul2708.quiz.util.MessageUtil;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.api.ServerAPI;
import de.paul2708.spigot.event.PlayerLoadedEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Paul on 02.02.2017.
 */
public class PlayerLoadedListener implements Listener {

    private boolean prepare = false;

    @EventHandler
    public void onLoaded(PlayerLoadedEvent e) {
        Player p = e.getPlayer();

        // Setup
        if (ServerAPI.getState() == GameState.SETUP) {
            MessageUtil.sendMessageToAll(Constants.TAG + "§eDer Konfigurant " + PlayerAPI.getRank(p).getPrefix() + p.getName() + " §ehat das Spiel betreten.");
            return;
        }

        // Lobby
        if(ServerAPI.getState() == GameState.LOBBY) {
            Stats.getStatistic(p).request();

            if (Voting.isActive()) {
                ItemManager.giveVoteItems(p);
            }

            MessageUtil.sendMessageToAll(Constants.TAG + PlayerAPI.getRank(p).getPrefix() + p.getName() + " §eist dem Spiel beigetreten §8[§e"
                    + Bukkit.getOnlinePlayers().size() + "§7/§e" + Bukkit.getServer().getMaxPlayers() + "§8]");

            Bukkit.getScheduler().scheduleSyncDelayedTask(Quiz.getInstance(), () -> {
                if (PlayerAPI.getRank(p).getPermissionLevel() >= 50) {
                    p.sendMessage(" ");
                    p.sendMessage(Constants.TAG + "§7Da du " + PlayerAPI.getRank(p).getPrefix() + PlayerAPI.getRank(p).getName() + " §7bist, kannst du das Spiel mit §c/start §7starten!");
                    p.sendMessage(" ");
                }
            }, 20);

        }
    }
}
