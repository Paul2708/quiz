package de.paul2708.quiz.listener.lobby;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 16.06.2016.
 */
public class VotingInventoryListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if(e.getWhoClicked() instanceof Player) {
            if(e.getInventory() == null || e.getInventory().getTitle() == null) return;
            if(ServerAPI.getState() != GameState.LOBBY) return;
            ItemStack item = e.getCurrentItem();
            if(item == null || item.getType() == null) return;
            if(item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null) return;

            e.setCancelled(true);

            Player p = (Player) e.getWhoClicked();
            String name = ChatColor.stripColor(e.getInventory().getTitle());

            // Map-Voting
            if(name.equalsIgnoreCase("Map-Voting")) {
                String mapName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
                MapInfo map = MapInfo.getInfoByName(mapName);

                if (!Voting.isActive()) {
                    p.sendMessage(Constants.TAG + "§cDu kannst nicht mehr voten.");
                    return;
                }

                Voting.vote(p, map);
                p.sendMessage(Constants.TAG + "§bDu hast für die Map §6" + map.getName() + " §bgestimmt.");
                e.getView().close();
                return;
            }
            // Map-Forcing
            if(name.equalsIgnoreCase("Map-Forcing")) {
                String mapName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
                MapInfo map = MapInfo.getInfoByName(mapName);

                Voting.force(map);
                p.sendMessage(Constants.TAG + "§bDu hast die Map §c" + map.getName() + " §bgeforcet.");
                e.getView().close();
            }
        }
    }
}
