package de.paul2708.quiz.listener.lobby;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.ItemManager;
import de.paul2708.quiz.util.Permission;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 16.06.2016.
 */
public class VotingInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Action action = e.getAction();
        ItemStack item = e.getItem();

        if(item == null || item.getType() == null) return;
        if(item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null) return;
        if(item.getType() == Material.AIR) return;
        if(ServerAPI.getState() != GameState.LOBBY) return;

        e.setCancelled(true);

        String name = ChatColor.stripColor(item.getItemMeta().getDisplayName());

        if(action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
            // Map-Voting
            if(item.getType() == Material.BOOK) {
                if(!Voting.isActive()) {
                    p.sendMessage(Constants.TAG + "§cDu kannst nicht mehr voten.");
                    return;
                }
                if(Voting.hasVoted(p)) {
                    p.sendMessage(Constants.TAG + "§cDu hast bereits gevotet.");
                    return;
                }

                ItemManager.openVotingInventory(p);
            }
            // Map-Forcing
            if(item.getType() == Material.PAPER) {
                if (PlayerAPI.getRank(p).getPermissionLevel() < Permission.MAP_FORCE)return;

                ItemManager.openForcingInventory(p);
            }
        }
    }
}
