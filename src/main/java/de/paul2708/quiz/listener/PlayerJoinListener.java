package de.paul2708.quiz.listener;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.scoreboard.LobbyScoreboard;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.Util;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Paul on 11.06.2016.
 */
public class PlayerJoinListener implements Listener {

    private boolean prepare = false;

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        // Setup
        if (ServerAPI.getState() == GameState.SETUP) {
            p.sendMessage(Constants.TAG + "§7Der Server befindet sich im Setup-Mode.");
            if (Quiz.getConfigFile().getLobby().equalsIgnoreCase("none")) {
                p.sendMessage(Constants.TAG + "§7Es wurde noch kein Lobby-Spawn gesetzt.");
            }
        }
        // Lobby
        if(ServerAPI.getState() == GameState.LOBBY) {
            if (!Game.getLobbyCountdown().isRunning()) {
                if (Bukkit.getOnlinePlayers().size() >= Game.getMinPlayers()) {
                    Game.getLobbyCountdown().start();
                }
            }

            Util.preparePlayer(p);

            LobbyScoreboard.set(p);

            if(Game.getLobby() == null) {
                p.sendMessage(Constants.TAG + "§7Es wurde noch kein Lobby-Spawn gesetzt.");
            } else {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Quiz.getInstance(), () -> {
                    p.teleport(Game.getLobby());
                    if (!prepare) {
                        Util.prepareWorld(p.getWorld().getName());
                        prepare = true;
                    }
                }, 10L);
            }

            e.setJoinMessage("");
        }
    }
}
