package de.paul2708.quiz.listener;

import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.util.Constants;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Paul on 16.06.2016.
 */
public class AsyncPlayerChatListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat (AsyncPlayerChatEvent e){
        Player p = e.getPlayer();

        if (Game.getRound() != null && !Game.getRound().canWrite()) {
            p.sendMessage(Constants.TAG + "§7Du kannst beim Raten nicht schreiben.");
            e.setCancelled(true);
            return;
        }
    }
}
