package de.paul2708.quiz.listener;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.game.Profile;
import de.paul2708.quiz.scoreboard.GameScoreboard;
import de.paul2708.quiz.stats.Stats;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.Util;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;

/**
 * Created by Paul on 16.06.2016.
 */
public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        // Setup
        if(ServerAPI.getState() == GameState.SETUP) {
            e.setQuitMessage(Constants.TAG + "§eDer Konfigurant " + PlayerAPI.getRank(p).getPrefix() + p.getName() + " §ehat das Spiel verlassen.");
        }
        // Lobby
        if(ServerAPI.getState() == GameState.LOBBY) {
            // Countdown
            if (Game.getLobbyCountdown().isRunning()) {
                if((Bukkit.getOnlinePlayers().size() - 1) < Game.getMinPlayers()) {
                    Bukkit.broadcastMessage(Constants.TAG + "§bDer Countdown wurde gestoppt, da zu wenig Spieler online sind");
                    Game.getLobbyCountdown().cancel();
                }
            }

            e.setQuitMessage(Constants.TAG + PlayerAPI.getRank(p).getPrefix() + p.getName() + " §ehat das Spiel verlassen §8[§e" + (Bukkit.getOnlinePlayers().size() -1) + "§7/§e" + Bukkit.getServer().getMaxPlayers() + "§8]");
        }
        // Ingame
        if(ServerAPI.getState() == GameState.INGAME) {
            if (Game.ended) return;

            GameScoreboard.deletePlayer(p);
            Profile profile = Profile.getProfile(p);

            Stats.getStatistic(p).update("Punkte", profile.getPoints());
            Stats.getStatistic(p).update("Richtig", profile.getCorrectAnswers());
            Stats.getStatistic(p).update("Falsch", profile.getWrongAnswers());

            Stats.getStatistic(p).push();

            Profile.remove(p);
            Game.getRound().removePlayer(p);

            e.setQuitMessage(Constants.TAG + PlayerAPI.getRank(p).getPrefix() + p.getName() + " §ehat das Spiel verlassen");

            if (Bukkit.getOnlinePlayers().size() - 1 == 1) {
                List<Profile> profiles = Util.sort(Profile.getProfiles(), true);
                Game.finish(profiles.get(0).getPlayer());
            } else if (Bukkit.getOnlinePlayers().size() - 1 == 0) {
                Bukkit.shutdown();
            }
        }
    }
}
