package de.paul2708.quiz.listener.setup;

import de.paul2708.quiz.Quiz;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Paul on 10.06.2016.
 */
public class SetupBlockBreakListener implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();

        if (!Quiz.getConfigFile().isInSetup()) return;
        if (!p.isOp()) return;

        if (p.getItemInHand() == null || p.getItemInHand().getType() == Material.AIR) return;
        Material type = p.getItemInHand().getType();

        if (type == Material.RAW_FISH || type == Material.BONE || type == Material.BLAZE_ROD ||
                type == Material.STICK) {
            e.setCancelled(true);
        }
    }
}
