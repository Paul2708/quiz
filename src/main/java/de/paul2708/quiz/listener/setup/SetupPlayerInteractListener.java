package de.paul2708.quiz.listener.setup;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.file.MapFile;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.LocationUtil;
import de.paul2708.spigot.api.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 08.06.2016.
 */
public class SetupPlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if (!p.isOp()) return;
        if (e.getAction() == Action.PHYSICAL) return;
        if (!Quiz.getConfigFile().isInSetup()) return;

        if (e.getItem() == null || e.getItem().getType() == null) return;
        if (e.getItem().getItemMeta() == null || e.getItem().getItemMeta().getDisplayName() == null) return;

        Material material = e.getItem().getType();
        String name = ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName());
        String world = name.split("Map=")[1].replaceAll("]", "");
        MapFile file = MapInfo.getInfo(world).getFile();

        // Spawn
        if (material == Material.BONE) {
            Location loc = p.getLocation();
            file.set("spawn", LocationUtil.getLocationAsString(loc));
            file.save();
            p.sendMessage(Constants.TAG + "§7Du hast den Spawn gesetzt.");
        }
        // Number
        if (material == Material.BLAZE_ROD) {
            String number = name.split(" ")[5];
            if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
                file.set("number." + number, new ArrayList<>());
                file.save();
                p.sendMessage(Constants.TAG + "§7Du hast alle Blöcke für §eNummer " + number + " §7gelöscht.");
            }
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                List<String> list = file.get("number." + number);
                list.add(LocationUtil.getLocationAsString(e.getClickedBlock().getLocation()));
                file.set("number." + number, list);
                file.save();
                p.sendMessage(Constants.TAG + "§7Du hast den Block zu Number §e" + number + " §7hinzugefügt.");
            }
        }
        // Area
        if (material == Material.STICK) {
            int currentBorder = Integer.valueOf(name.split(" ")[1]);
            String number = name.split(" ")[4];
            if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR) {
                ItemStack area = new ItemBuilder().name("Begrenzung " + (currentBorder == 1 ? "2" : "1") + " der Antwort " + number + " [Map=" + world + "]").description("Rechtsklick: geklickter Block")
                        .description("Linksklick: Nächster Block").description("Sneake um die nächste Begrenzung zu setzen")
                        .type(Material.STICK).build();

                p.setItemInHand(area);
                p.sendMessage(Constants.TAG + "§7Du hast nun das Item für Begrenzung §e" + (currentBorder == 1 ? "2" : "1") + "§7.");
            }
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                file.set("area." + number + "." + currentBorder, LocationUtil.getLocationAsString(e.getClickedBlock().getLocation()));
                file.save();
                p.sendMessage(Constants.TAG + "§7Du hast nun den Block für Begrenzung §e" + (currentBorder == 1 ? "1" : "2") + " §7gesetzt.");
            }
        }
        // Hologram
        if (material == Material.RAW_FISH) {
            String number = name.split(" ")[5];
            file.set("hologram." + number, LocationUtil.getLocationAsString(p.getLocation()));
            file.save();
            p.sendMessage(Constants.TAG + "§7Du hast das Hologram für Antwort §e" + number + " §7gesetzt.");
        }
    }
}
