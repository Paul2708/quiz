package de.paul2708.quiz.listener.setup;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 10.06.2016.
 */
public class SetupPlayerToggleSneakListener implements Listener {

    @EventHandler
    public void onSneak(PlayerToggleSneakEvent e) {
        Player p = e.getPlayer();

        if(Quiz.getConfigFile().isInSetup()) {
            if(p.isSneaking()) return;
            ItemStack item = p.getItemInHand();
            if(item == null || item.getType() == null) return;
            if(item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null) return;
            if(item.getType() == Material.AIR) return;

            String name = ChatColor.stripColor(item.getItemMeta().getDisplayName());
            String world = name.split("Map=")[1].replaceAll("]", "");

            // Number
            if(item.getType() == Material.BLAZE_ROD) {
                int number = Integer.valueOf(name.split(" ")[5]);
                number = number + 1;
                if (number == 4) number = 1;
                ItemStack itemStack = new ItemBuilder().name("Position der Lampen für Antwort " + number + " [Map=" + world + "]").description("Rechtsklick: geklickter Block")
                        .description("Linksklick: Löschen").description("Sneake um die nächste Nummber zu setzen")
                        .type(Material.BLAZE_ROD).build();

                p.setItemInHand(itemStack);
                p.sendMessage(Constants.TAG + "§7Du hast nun das Item für die Lampen der §e" + number + ". Antwort§7.");
            }
            // Area
            if(item.getType() == Material.STICK) {
                int number = Integer.valueOf(name.split(" ")[4]);
                number = number + 1;
                if (number == 4) number = 1;
                ItemStack area = new ItemBuilder().name("Begrenzung 1 der Antwort " + number + " [Map=" + world + "]").description("Rechtsklick: geklickter Block")
                        .description("Linksklick: Nächster Block").description("Sneake um die nächste Begrenzung zu setzen")
                        .type(Material.STICK).build();

                p.setItemInHand(area);
                p.sendMessage(Constants.TAG + "§7Du hast nun das Item für die Begrenzung der §e" + number + ". Antwort§7.");
            }
            // General
            if(item.getType() == Material.RAW_FISH) {
                int number = Integer.valueOf(name.split(" ")[5]);
                number = number + 1;
                if (number == 4) number = 1;
                ItemStack holo = new ItemBuilder().name("Position des Holograms für Antwort " + number + " [Map=" + world + "]").description("Links u. Rechtsklick:")
                        .description("aktuelle Spielerposition").description("Sneake um die nächste Antwort zu setzen")
                        .type(Material.RAW_FISH).build();

                p.setItemInHand(holo);
                p.sendMessage(Constants.TAG + "§7Du hast nun das Item für das Hologram der §e" + number + ". Antwort§7.");
            }
        }
    }
}
