package de.paul2708.quiz.game;

import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.scoreboard.LobbyScoreboard;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Paul on 11.06.2016.
 */
public class Voting {

    private static HashMap<MapInfo, Integer> votes = new HashMap<>();
    public static List<UUID> voter = new ArrayList<>();

    private static boolean active = true;

    private static MapInfo finalMap;

    public static void selectMaps() {
        List<MapInfo> maps = MapInfo.getMaps().stream().filter(MapInfo::isEnabled).collect(Collectors.toList());

        Collections.shuffle(maps);

        if (maps.size() >= 3) {
            votes.put(maps.get(0), 0);
            votes.put(maps.get(1), 0);
            votes.put(maps.get(2), 0);
        } else if (maps.size() == 1) {
            votes.put(maps.get(0), 0);
        } else {
            votes.put(maps.get(0), 0);
            votes.put(maps.get(1), 0);
        }
    }

    public static void force(MapInfo map) {
        active = false;
        votes.clear();
        votes.put(map, 1);
        LobbyScoreboard.deleteScores(map);
    }

    public static void chooseFinalMap() {
        MapInfo map = null;
        int maxVotes = -1;

        for (Map.Entry<MapInfo, Integer> entry : votes.entrySet()) {
            if (Math.max(maxVotes, entry.getValue()) == entry.getValue()) {
                map = entry.getKey();
                maxVotes = entry.getValue();
            }
        }

        finalMap = map;
    }

    public static void vote(Player p, MapInfo map) {
        int vote = votes.get(map);
        vote = vote + 1;
        votes.put(map, vote);
        voter.add(p.getUniqueId());
        LobbyScoreboard.editVote(map);
    }

    public static boolean hasVoted(Player p) {
        return voter.contains(p.getUniqueId());
    }

    public static int getVotes(MapInfo map) {
        return votes.get(map);
    }

    public static void enable(boolean active) {
        Voting.active = active;
    }

    public static MapInfo getMap() {
        return finalMap;
    }

    public static boolean isActive() {
        return active;
    }

    public static List<MapInfo> getMaps() {
         List<MapInfo> maps = new ArrayList<>();

        maps.addAll(votes.keySet());

         return maps;
     }
}
