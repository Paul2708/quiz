package de.paul2708.quiz.game;

import de.paul2708.quiz.util.Util;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Paul on 18.06.2016.
 */
public class Area {

    private int id;

    private Location a, b;
    private List<Location> locations;

    public Area(int id, Location a, Location b) {
        this.id = id;
        this.a = a;
        this.b = b;

        this.locations = Util.select(a, b, a.getWorld());
    }

    public Location getCenter() {
        return new Location(a.getWorld(), (a.getX() + b.getX()) / 2, a.getY(), (a.getZ() + b.getZ()) / 2);
    }

    public boolean isIn(Player p) {
        Location loc = p.getLocation();
        for (Location location : locations) {
            if (location.getBlockX() == loc.getBlockX() &&
                    location.getBlockZ() == loc.getBlockZ()) {
                return true;
            }
        }

        return false;
    }

    public int getId() {
        return id;
    }

    public List<Location> getArea() {
        return locations;
    }

    public static boolean isIn(Area[] areas, Player p) {
        for (Area area : areas) {
            if (area.isIn(p)) {
                return true;
            }
        }

        return false;
    }
}
