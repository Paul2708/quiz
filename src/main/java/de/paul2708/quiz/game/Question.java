package de.paul2708.quiz.game;

import de.paul2708.quiz.Quiz;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 17.06.2016.
 */
public class Question {

    private String question;
    private String answer1, answer2, answer3;

    public Question(String question, String answer1, String answer2, String answer3) {
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getAnswers() {
        List<String> answers = new ArrayList<>();
        answers.add(answer1);
        answers.add(answer2);
        answers.add(answer3);
        return answers;
    }

    private static List<Question> questions;

    public static List<Question> getAllQuestions() {
        if (questions != null) {
            return questions;
        }

        questions = new ArrayList<>();

        ResultSet rs = Quiz.getMySQL().getQuery("SELECT * FROM `questions`");
        try {
            while (rs.next()) {
                String question = rs.getString("question");
                String answer1 = rs.getString("answer_1");
                String answer2 = rs.getString("answer_2");
                String correct = rs.getString("correct_answer");

                questions.add(new Question(question, answer1, answer2, correct));
            }
        } catch(SQLException e) {
            System.out.println("Fehler beim Bekommen der Fragen.");
            e.printStackTrace();
        }

        return questions;
    }
}
