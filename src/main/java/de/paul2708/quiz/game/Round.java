package de.paul2708.quiz.game;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.countdown.MessageCountdown;
import de.paul2708.quiz.countdown.RoundCountdown;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.scoreboard.GameScoreboard;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.Util;
import de.paul2708.spigot.api.Hologram;
import de.paul2708.spigot.api.TokensAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by Paul on 16.06.2016.
 */
public class Round {

    private int id;

    private Question question;

    private boolean write, move, useItems;

    private List<Hologram> holograms;

    private HashMap<String, Integer> answers;

    private RoundCountdown countdown;
    private MessageCountdown messageCountdown;

    private Area[] areas;

    public Round(int id) {
        this.id = id;
    }

    public void setup() {
        light(1, false);
        light(2, false);
        light(3, false);

        this.write = true;
        this.move = true;
        this.useItems = false;

        this.question = Game.getQuestions().get(id - 1);
        this.holograms = new ArrayList<>();
        this.answers = new HashMap<>();

        this.countdown = new RoundCountdown(this, 40);

        this.areas = new Area[] { new Area(1, Voting.getMap().getArea(1)[0],
                Voting.getMap().getArea(1)[1]),
            new Area(2, Voting.getMap().getArea(2)[0], Voting.getMap().getArea(2)[1]),
            new Area(3, Voting.getMap().getArea(3)[0], Voting.getMap().getArea(3)[1]) };

        GameScoreboard.update(id, 20);

        for (Profile profile : Profile.getProfiles()) {
            profile.getPlayer().setLevel(profile.getRank());
            profile.visible(false);
        }

        makePlayerInvisible(true);
    }

    public void light(int number, boolean light) {
        for (Location location : MapInfo.getInfo(Voting.getMap().getWorldName()).getNumber(number)) {
            if (location.getBlock() == null) continue;
            location.getBlock().setType(light ? Material.GLOWSTONE : Material.REDSTONE_LAMP_OFF);
        }
    }

    public void sendTutorial() {
        write(false);
        List<String> messages = new ArrayList<>();
        messages.add(Constants.TAG + "§7Willkommen bei dem OzeanGames-Quiz.");
        messages.add(Constants.TAG + "§7Euch werden §e10 Fragen §7gestellt mit §e3 möglichen Antworten§7.");
        messages.add(Constants.TAG + "§7Ihr habt §e20 Sekunden Zeit §7um auf der richtigen Antwort zu stehen.");
        messages.add(Constants.TAG + "§7Und ob ihr wirklich richtig steht, seht ihr wenn das Licht angeht.");
        messages.add(Constants.TAG + "§eViel Erfolg!");

        messageCountdown = new MessageCountdown(messages, 2);
        messageCountdown.start();
    }

    public void start() {
        if (id == 1) {
            clearChat();

            Bukkit.getScheduler().runTaskLaterAsynchronously(Quiz.getInstance(), () -> {
                MessageCountdown msgCd = new MessageCountdown(Arrays.asList(Constants.TAG + "§7" + id + ". Frage",
                        Constants.TAG + "§e" + question.getQuestion()), 1);
                msgCd.start();

            }, (messageCountdown.getStartTime() + 1) * 20);
            Bukkit.getScheduler().runTaskLaterAsynchronously(Quiz.getInstance(), () -> {
                write(false);
                countdown.start();
                spawnAnswers();
                makePlayerInvisible(false);
                useItems = true;
            }, (messageCountdown.getStartTime() + 1) * 20 + 50);
        } else {
            clearChat();

            Bukkit.getScheduler().runTaskLaterAsynchronously(Quiz.getInstance(), new Runnable() {

                @Override
                public void run() {
                    MessageCountdown msgCd = new MessageCountdown(Arrays.asList(Constants.TAG + "§7" + id + ". Frage",
                            Constants.TAG + "§e" + question.getQuestion()), 1);
                    msgCd.start();
                }
            }, 20L);
            Bukkit.getScheduler().runTaskLaterAsynchronously(Quiz.getInstance(), () -> {
                write(false);
                countdown.start();
                spawnAnswers();
                makePlayerInvisible(false);
                useItems = true;
            }, 70L);
        }
    }

    public void finish() {
        Util.sendActionBar("&7Antwort: &e" + question.getAnswers().get(2));
        Bukkit.broadcastMessage(Constants.TAG + "§7Richtige Antwort: §e" + question.getAnswers().get(2));
        light(getNumber(question.getAnswers().get(2)), true);
        move(true);
        write(true);
        useItems = false;
        int correct = getNumber(question.getAnswers().get(2));

        Util.spawnFirework(areas[correct - 1].getCenter().add(0, 2.88, 0));

        for (Profile profile : Profile.getProfiles()) {
            Player p = profile.getPlayer();
            if (profile.isJokerActive()) {
                p.sendMessage(Constants.TAG + "§eDa dein Joker aktiv ist, bekommst du keine Punkte dazu/abgezogen.");
                profile.joker(false);
                continue;
            }
            if (areas[correct - 1].isIn(p)) {
                p.sendMessage(Constants.TAG + "§aDeine Antwort war richtig. (+2 Punkte)");
                profile.addPoints(2);
                profile.correctAnswer();
                TokensAPI.updateTokens(p, 5, "Richtige Anwort");
            } else if (Area.isIn(areas, p)){
                p.sendMessage(Constants.TAG + "§cDeine Antwort war nicht korrekt. (- 1 Punkt)");
                profile.addPoints(-1);
                profile.wrongAnswer();
            } else {
                p.sendMessage(Constants.TAG + "§cDu bist auf keinem Feld. (- 1 Punkt)");
                profile.addPoints(-1);
                profile.wrongAnswer();
            }
        }

        GameScoreboard.updateScores();
        GameScoreboard.update(id++, 20);
    }

    public void write(boolean write) {
        this.write = write;
    }

    public void move(boolean move) {
        this.move = move;
    }

    public void removePlayer(Player p) {
        this.holograms.remove(p);
    }

    public Round next() {
        int newRound = id++;
        Round round = new Round(newRound);
        Game.setRound(round);
        return round;
    }

    public boolean canWrite() {
        return write;
    }

    public boolean canMove() {
        return move;
    }

    public boolean canUseItems() {
        return useItems;
    }

    public Question getQuestion() {
        return question;
    }

    public int getNumber(String answer) {
        return answers.get(answer);
    }

    public RoundCountdown getCountdown() {
        return countdown;
    }

    public MessageCountdown getMessageCountdown() {
        return messageCountdown;
    }

    public int getId() {
        return id;
    }

    public void makePlayerInvisible(boolean show) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Quiz.getInstance(), new Runnable() {

            @Override
            public void run() {
                for (Profile profile : Profile.getProfiles()) {
                    Player p = profile.getPlayer();

                    for (Player all : Bukkit.getOnlinePlayers()) {
                        if (show) {
                            p.showPlayer(all);
                        } else {
                            p.hidePlayer(all);
                        }
                    }
                    if (profile.isVisibleActive()) {
                        Bukkit.getOnlinePlayers().forEach(p::showPlayer);
                    }
                }
            }
        });
    }

    private void clearChat() {
        for (int i = 0; i < 100; i++) {
            Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(" "));
        }
    }

    private void spawnAnswers() {
        List<String> answers = question.getAnswers();
        Collections.shuffle(answers);

        for (int i = 1; i < 4; i++) {
            Location location = MapInfo.getInfo(Voting.getMap().getWorldName()).getHologram(i);
            String answer = answers.get(i - 1);
            Hologram hologram = new Hologram(location, Arrays.asList("§7" + i + ". Antwort:", "§e" + answer));
            Bukkit.getOnlinePlayers().forEach(p -> hologram.display(p));

            holograms.add(hologram);
            this.answers.put(answer, i);
        }
    }

    public void despawnAnswers() {
        for (Hologram hologram : holograms) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                hologram.destroy(player);
            }
        }
    }
}
