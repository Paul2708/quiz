package de.paul2708.quiz.game;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Paul on 17.06.2016.
 */
public class Profile {

    private static HashMap<Player, Profile> cache = new HashMap<>();

    private Player player;

    private int rank;
    private int points;
    private int correctAnswers;
    private int wrongAnswers;

    private boolean joker;
    private boolean visible;

    public Profile(Player player) {
        this.player = player;

        this.rank = 0;
        this.points = 0;
        this.correctAnswers = 0;
        this.wrongAnswers = 0;

        this.joker = false;
        this.visible = false;

        Profile.cache.put(player, this);
    }

    public void correctAnswer() {
        correctAnswers++;
    }

    public void wrongAnswer() {
        wrongAnswers++;
    }

    public void joker(boolean joker) {
        this.joker = joker;
    }

    public void visible(boolean visible) {
        this.visible = visible;
    }

    public void addPoints(int increase) {
        this.points += increase;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public int getWrongAnswers() {
        return wrongAnswers;
    }

    public boolean isJokerActive() {
        return joker;
    }

    public boolean isVisibleActive() {
        return visible;
    }

    public int getRank() {
        return rank;
    }

    public int getPoints() {
        return points;
    }

    public String getName() {
        return player.getName();
    }

    public Player getPlayer() {
        return player;
    }

    public static void remove(Player p) {
        Profile.cache.remove(p);
    }

    public static Profile getProfile(Player p) {
        return cache.get(p);
    }

    public static List<Profile> getProfiles() {
        List<Profile> list = new ArrayList<>();
        list.addAll(cache.values());

        return list;
    }

}
