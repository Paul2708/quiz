package de.paul2708.quiz.game;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.countdown.LobbyCountdown;
import de.paul2708.quiz.countdown.RestartCountdown;
import de.paul2708.quiz.scoreboard.GameScoreboard;
import de.paul2708.quiz.stats.Stats;
import de.paul2708.quiz.util.*;
import de.paul2708.spigot.api.PlayerAPI;
import de.paul2708.spigot.api.TokensAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Paul on 10.06.2016.
 */
public class Game {

    private static int minPlayers, maxPlayers;
    private static Location lobby;

    private static LobbyCountdown lobbyCountdown;

    private static Round round;

    private static List<Question> questions;

    public static boolean ended = false;

    public static void initialize()  {
        minPlayers = Quiz.getConfigFile().getMinPlayers();
        maxPlayers = Quiz.getConfigFile().getMaxPlayers();

        GameScoreboard.nextToName = Quiz.getConfigFile().isNextToName();

        if (Quiz.getConfigFile().getLobby().equalsIgnoreCase("none")) {
            lobby = null;
        } else {
            lobby = LocationUtil.getLocationFromString(Quiz.getConfigFile().getLobby());
        }

        lobbyCountdown = new LobbyCountdown(60);

        questions = new ArrayList<>();
        Bukkit.getScheduler().scheduleSyncDelayedTask(Quiz.getInstance(), () -> {
            selectQuestions();
            System.out.println(Constants.CONSOLE + questions.size() + " Fragen geladen.");
        }, 20*5L);
    }

    public static void finish(Player p) {
        if (Game.getRound() != null) {
            if (Game.getRound().getCountdown().isRunning()) Game.getRound().getCountdown().cancel();
            if (Game.getRound().getMessageCountdown() != null && Game.getRound().getMessageCountdown().isRunning()) Game.getRound().getMessageCountdown().cancel();
        }

        Game.ended = true;

        Util.clearChat();
        MessageUtil.sendMessageToAll(Constants.TAG + PlayerAPI.getRank(p).getPrefix() + p.getName() + " §bhat die Runde gewonnen! §7(" + Profile.getProfile(p).getPoints() + " Punkte)");
        TokensAPI.updateTokens(p, 100, "Game Win");

        for (Profile profile : Profile.getProfiles()) {
            Player player = profile.getPlayer();
            player.sendMessage(Constants.TAG + "§7Deine Runden-Statistik:");
            player.sendMessage(Constants.TAG + "§7Rang: §e" + profile.getRank());
            player.sendMessage(Constants.TAG + "§7Punkte: §e" + profile.getPoints());
            player.sendMessage(Constants.TAG + "§7Richtige Antworten: §e" + profile.getCorrectAnswers());
            player.sendMessage(Constants.TAG + "§7Falsche Antworten: §e" + profile.getWrongAnswers());

            Stats.getStatistic(player).update("Spiele", 1);

            if (profile.getRank() == 1) {
                Stats.getStatistic(player).update("Siege", 1);
            }

            Stats.getStatistic(player).update("Punkte", profile.getPoints());
            Stats.getStatistic(player).update("Richtig", profile.getCorrectAnswers());
            Stats.getStatistic(player).update("Falsch", profile.getWrongAnswers());

            Stats.getStatistic(player).push();

            ItemManager.clearInventory(player);
        }

        new RestartCountdown(15).start();
    }

    public static int getMinPlayers() {
        return minPlayers;
    }

    public static int getMaxPlayers() {
        return maxPlayers;
    }

    public static Location getLobby() {
        return lobby;
    }

    public static LobbyCountdown getLobbyCountdown() {
        return lobbyCountdown;
    }

    public static void setRound(Round round) {
        Game.round = round;
    }

    public static Round getRound() {
        return round;
    }

    public static List<Question> getQuestions() {
        return questions;
    }

    private static void selectQuestions() {
        List<Question> temp = Question.getAllQuestions();
        Collections.shuffle(temp);
        for (int i = 0; i < 11; i++) {
            questions.add(temp.get(i));
        }
    }
}
