package de.paul2708.quiz.map;

import de.paul2708.quiz.file.MapFile;
import de.paul2708.quiz.util.LocationUtil;
import de.paul2708.quiz.util.Util;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Paul on 07.06.2016.
 */
public class MapInfo {

    public static HashMap<String, MapInfo> cache = new HashMap<>();

    private String worldName;
    private MapFile file;

    public MapInfo(String worldName) {
        this.worldName = worldName;
        this.file = new MapFile(worldName);

        cache.put(worldName, this);
    }

    public boolean isEnabled() {
        return file.get("enable");
    }

    public String getWorldName() {
        return file.get("world_name");
    }

    public String getName() {
        return file.get("display_name");
    }

    public String getBuilder() {
        return file.get("builder");
    }

    public int[] getIconID() {
        String iconMaterial = file.get("icon_material");
        if(iconMaterial.contains(":")) {
            String[] ids = iconMaterial.split(":");
            return new int[] { Integer.valueOf(ids[0]), Integer.valueOf(ids[1]) };
        } else {
            return new int[] { Integer.valueOf(iconMaterial), 0 };
        }
    }

    public List<Location> getNumber(int number) {
        List<String> sLoc = file.get("number." + number);
        List<Location> loc = new ArrayList<>();

        for (String s : sLoc) {
            System.out.println("Location: " + s);
            loc.add(LocationUtil.getLocationFromString(s));
        }


        return loc;
    }

    public Location[] getArea(int number) {
        Location[] array = new Location[2];
        array[0] = LocationUtil.getLocationFromString(file.get("area." + number + ".1"));
        array[1] = LocationUtil.getLocationFromString(file.get("area." + number + ".2"));

        return array;
    }

    public Location getSpawn() {
        return LocationUtil.getLocationFromString(file.get("spawn"));
    }

    public Location getHologram(int number) {
        return LocationUtil.getLocationFromString(file.get("hologram." + number));
    }

    public int getTeleportY() {
        return file.get("teleport_height");
    }

    public MapFile getFile() {
        return file;
    }

    public static MapInfo getInfo(String worldName) {
        if(cache.get(worldName) == null) {
            MapInfo info = new MapInfo(worldName);
            return info;
        } else {
            return cache.get(worldName);
        }
    }

    public static MapInfo getInfoByName(String name) {
        for (String names : Util.getAllFiles()) {
            MapInfo info = getInfo(names);
            if(info.getName().equalsIgnoreCase(name)) {
                return info;
            }
        }

        return null;
    }

    public static List<MapInfo> getMaps() {
        List<MapInfo> info = new ArrayList<>();
        for (String names : Util.getAllFiles()) {
            info.add(MapInfo.getInfo(names));
        }

        return info;
    }

    public static boolean doesMapExist(String worldName) {
        for (String names : Util.getAllFiles()) {
            if (names.equalsIgnoreCase(worldName)) {
                return true;
            }
        }

        return false;
    }
}
