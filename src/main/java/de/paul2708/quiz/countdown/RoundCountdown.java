package de.paul2708.quiz.countdown;

import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.game.Profile;
import de.paul2708.quiz.game.Round;
import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.scoreboard.GameScoreboard;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.MessageUtil;
import de.paul2708.quiz.util.Util;
import de.paul2708.spigot.api.BasicCountdown;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Paul on 17.06.2016.
 */
public class RoundCountdown extends BasicCountdown {

    private Round round;

    private int staticTime;

    public RoundCountdown(Round round, int time) {
        super(time);
        this.round = round;
        this.staticTime = time;
    }

    @Override
    public void handle(int time) {
        // Game
        if (time >= 20) {
            GameScoreboard.update(round.getId(), (time - 20));
            Util.sendActionBar("&7Frage: &e" + round.getQuestion().getQuestion());

            for (Player p : Bukkit.getOnlinePlayers()) {
                p.setExp((time-20) * 1.0f / (staticTime - 20) * 1.0f);
            }
        }
        if (time == 35 || time == 30 || time == 25 || time == 23 || time == 22 || time == 21) {
            MessageUtil.sendMessageToAll(Constants.TAG + "§7Ihr habt noch §e" + (time-20) + " Sekunde" + (time == 21 ? "" : "n") + ".");
            Bukkit.getOnlinePlayers().forEach(p -> p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 1));
        }
        if (time == 20) {
            round.makePlayerInvisible(true);
            MessageUtil.sendMessageToAll(Constants.TAG + "§eUnd ob ihr wirklich richtig steht...");
            round.write(true);
            round.move(false);
        }
        if (time == 15) {
            MessageUtil.sendMessageToAll(Constants.TAG + "§eseht ihr, wenn das Licht angeht.");
        }
        if (time == 13) {
            round.finish();
        }
        // TODO: Use vector
        /*if (time == 10) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                Vector to = Voting.getMap().getSpawn().toVector();
                Vector from = p.getLocation().toVector();
                Vector vector = to.subtract(from).multiply(2.5).add(new Vector(0, 1.25, 0));
                p.setVelocity(vector);
            }
        }*/
        if (time == 8) {
            round.light(1, false);
            round.light(2, false);
            round.light(3, false);

            round.despawnAnswers();

            for (Player p : Bukkit.getOnlinePlayers()) {
                p.teleport(Voting.getMap().getSpawn());
            }

            if (round.getId() + 1 == 12) {
                List<Profile> profiles = Util.sort(Profile.getProfiles(), true);
                int points = profiles.get(0).getPoints();
                List<Profile> winner = new ArrayList<>();

                for (Profile profile : profiles) {
                    if (profile.getPoints() == points) {
                        winner.add(profile);
                    }
                }

                Game.finish(winner.get(new Random().nextInt(winner.size())).getPlayer());
                cancel();
                return;
            }

            Round next = round.next();
            next.setup();
            next.start();

            cancel();
        }
    }
}
