package de.paul2708.quiz.countdown;

import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.MessageUtil;
import de.paul2708.spigot.api.BasicCountdown;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

/**
 * Created by Paul on 18.06.2016.
 */
public class RestartCountdown extends BasicCountdown {

    public RestartCountdown(int time) {
        super(time);
    }

    @Override
    public void handle(int time) {
        if (time == 15 || time == 10 || time <= 5) {
            MessageUtil.sendMessageToAll(Constants.TAG + "§7Der Server restartet in §e" + time + (time == 1 ? " Sekunde§7!" : " Sekunden§7!"));
            Bukkit.getOnlinePlayers().forEach(p -> p.playSound(p.getLocation(), Sound.NOTE_SNARE_DRUM, 1, 1));
        }
        if (time == 0) {
            Bukkit.getOnlinePlayers().forEach(p -> ServerAPI.sendToHub(p));
            Bukkit.shutdown();
        }
    }
}
