package de.paul2708.quiz.countdown;

import de.paul2708.quiz.util.MessageUtil;
import de.paul2708.quiz.util.Util;
import de.paul2708.spigot.api.BasicCountdown;
import org.bukkit.Bukkit;

import java.util.List;

/**
 * Created by Paul on 18.06.2016.
 */
public class MessageCountdown extends BasicCountdown {

    private List<String> messages;
    private int delay;

    public MessageCountdown(List<String> messages, int delay) {
        super(messages.size() * delay);

        this.messages = messages;
        this.delay = delay;
    }

    private int currentMessage = 0;

    @Override
    public void handle(int time) {
        if (time % delay == 0) {
            Util.clearChat();

            for (int i = 0; i <= currentMessage; i++) {
                MessageUtil.sendMessageToAll(messages.get(i));
            }
            for (int i = currentMessage; i < messages.size(); i++) {
                MessageUtil.sendMessageToAll(" ");
            }

            currentMessage++;
        }
    }

}
