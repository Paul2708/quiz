package de.paul2708.quiz.countdown;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.game.Profile;
import de.paul2708.quiz.game.Round;
import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.scoreboard.GameScoreboard;
import de.paul2708.quiz.scoreboard.LobbyScoreboard;
import de.paul2708.quiz.util.*;
import de.paul2708.spigot.api.BasicCountdown;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 11.06.2016.
 */
public class LobbyCountdown extends BasicCountdown {

    private int startTime;

    public LobbyCountdown(int time) {
        super(time);
        this.startTime = time;
    }

    @Override
    public void handle(int time) {
        LobbyScoreboard.update(time);

        Bukkit.getOnlinePlayers().forEach(p -> {
            p.setExp(time / startTime);
            p.setLevel(time);
        });

        if(time == 10) {
            if(Bukkit.getOnlinePlayers().size() < Game.getMinPlayers()) {
                MessageUtil.sendMessageToAll(Constants.TAG + "§cDer Countdown wurde gestoppt, da zu wenig Spieler online sind");
                cancel();
            }
        }
        if(time % 10 == 0 && time != 0) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                all.playSound(all.getLocation(), Sound.NOTE_PLING, 1, 1);
                all.sendMessage(Constants.TAG + "§7Das Spiel beginnt in §e"
                        + time + " Sekunden§7!");
            }
        }
        if(time == 5) {
            Voting.enable(false);
            Voting.chooseFinalMap();
            LobbyScoreboard.deleteScores(Voting.getMap());

            Bukkit.getOnlinePlayers().forEach(p -> {
                ItemManager.clearInventory(p);
                p.closeInventory();
                PacketUtil.sendTitle(p, 0, 60, 0, "§6" + Voting.getMap().getName(), "§7" + Voting.getMap().getBuilder());
            });
        }
        if(time == 5 || time == 4 || time == 3 || time == 2 || time == 1) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                all.sendMessage(Constants.TAG + "§7Das Spiel beginnt in §e" + time + " " + (time == 1 ? "Sekunde§7!" : "Sekunden§7!"));
                all.playSound(all.getLocation(), Sound.NOTE_PLING, 1, 1);
            }
        }
        if(time == 0) {
            if(Bukkit.getOnlinePlayers().size() < Game.getMinPlayers()) {
                MessageUtil.sendMessageToAll(Constants.TAG + "§7Der Countdown wurde gestoppt, da zu wenig Spieler online sind.");
                cancel();
                return;
            }

            ServerAPI.setGameState(GameState.INGAME);
            ServerAPI.setJoinAble(false);

            Util.prepareWorld(Voting.getMap().getWorldName());

            Bukkit.getOnlinePlayers().forEach(p -> {
                p.teleport(Voting.getMap().getSpawn());
                p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
                p.setLevel(0);
                ItemManager.giveGameItems(p);

                new Profile(p);
            });

            GameScoreboard.initializeScoreboard();

            Bukkit.getOnlinePlayers().forEach(p -> GameScoreboard.set(p));

            Game.setRound(new Round(1));

            Game.getRound().setup();
            Game.getRound().sendTutorial();
            Game.getRound().start();
        }
    }

    @Override
    public void cancel() {
        super.cancel();

        LobbyScoreboard.update(60);
        Bukkit.getOnlinePlayers().forEach(p -> {
            p.setExp(getRemainingTime() / startTime);
            p.setLevel(getRemainingTime());
        });
        if (Game.getRound() != null) {
            Bukkit.getOnlinePlayers().forEach(p -> {
                p.setExp(0);
                p.setLevel(0);
            });
        }
    }
}
