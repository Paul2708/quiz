package de.paul2708.quiz.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul on 11.06.2016.
 */
public class BasicScoreboard {

    private Scoreboard scoreBoard;
    private Objective objective;

    private int code;

    private Map<String, Integer> codes;

    public BasicScoreboard(String id, DisplaySlot slot) {
        this.scoreBoard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = scoreBoard.registerNewObjective(id, "dummy");
        objective.setDisplaySlot(slot);

        this.code = 0;

        this.codes = new HashMap<>();
    }

    public void setTitle(String title) {
        objective.setDisplayName(title);
    }

    public void addText(String type, String key, String value, int index) {
        Team score = scoreBoard.registerNewTeam(type);
        String prefix = key;
        String suffix = value;

        if (!prefix.startsWith("§")) {
            prefix = "§r" + key;
        }
        if (!suffix.startsWith("§")) {
            suffix = "§r" + value;
        }

        if (prefix.length() > 16) {
            prefix = prefix.substring(0, 16);
        }
        if (suffix.length() > 16) {
            suffix = suffix.substring(0, 16);
        }

        score.setPrefix(prefix);
        score.setSuffix(suffix);

        score.addEntry(ChatColor.values()[code].toString());

        objective.getScore(ChatColor.values()[code].toString()).setScore(index);
        codes.put(type, code);

        code++;
    }

    public void addText(String type, String key, int index) {
        addText(type, key, "", index);
    }

    public void editText(String type, String value) {
        Team score = scoreBoard.getTeam(type);
        String suffix = value;

        if (!suffix.startsWith("§")) {
            suffix = "§r" + value;
        }

        if (value.length() > 16) {
            suffix = suffix.substring(0, 16);
        }

        score.setSuffix(suffix);
    }

    public void editText(String type, String value, int index) {
        Team score = scoreBoard.getTeam(type);
        String suffix = "§r" + value;

        if (value.length() > 16) {
            suffix = suffix.substring(0, 16);
        }

        score.setSuffix(suffix);
        objective.getScore(ChatColor.values()[codes.get(type)].toString()).setScore(index);
    }

    public void editText(String type, String key, String value, int index) {
        Team score = scoreBoard.getTeam(type);
        String prefix = key;
        String suffix = value;

        if (!prefix.startsWith("§")) {
            prefix = "§r" + key;
        }
        if (!suffix.startsWith("§")) {
            suffix = "§r" + value;
        }

        if (prefix.length() > 16) {
            prefix = prefix.substring(0, 16);
        }
        if (value.length() > 16) {
            suffix = suffix.substring(0, 16);
        }

        score.setPrefix(prefix);
        score.setSuffix(suffix);
        objective.getScore(ChatColor.values()[codes.get(type)].toString()).setScore(index);
    }

    public void deleteText(String type) {
        scoreBoard.resetScores(ChatColor.values()[codes.get(type)].toString());
        scoreBoard.getTeam(type).unregister();
        codes.remove(type);
    }

    public void set(Player p) {
        p.setScoreboard(scoreBoard);
    }

    public boolean contains(String type) {
        for (Map.Entry<String, Integer> entry : codes.entrySet()) {
            if (entry.getKey().equalsIgnoreCase(type)) {
                return true;
            }
        }

        return false;
    }
}
