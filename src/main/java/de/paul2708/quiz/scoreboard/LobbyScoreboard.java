package de.paul2708.quiz.scoreboard;

import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

/**
 * Created by Paul on 11.06.2016.
 */
public class LobbyScoreboard {

    private static BasicScoreboard scoreboard;

    public static void initializeScoreboard() {
        scoreboard = new BasicScoreboard("lobby", DisplaySlot.SIDEBAR);
        scoreboard.setTitle("§bQuiz §8- §7" + Util.secToMin(60));
        scoreboard.addText("space", "         ", "        ", 16);
        scoreboard.addText("header", "§7" + "        " + ChatColor.UNDERLINE + "Map", "§7" + ChatColor.UNDERLINE + "wahl§r     ", 15);
        int score = 14;
        int i = 1;

        for (MapInfo map : Voting.getMaps()) {
            String first = map.getName();
            String second = "";
            if (map.getName().length() > 14) {
                first = map.getName().substring(0, 14);
                if (map.getName().length() >= 30) {
                    second = map.getName().substring(14, 30);
                } else {
                    second = map.getName().substring(14, (map.getName().length()));
                }
            }

            scoreboard.addText("map" + i, "§e" + first, "§e" + second, score--);
            scoreboard.addText("vote" + i, "§7Votes: ", "§e0", score--);
            i++;
        }
    }

    public static void editVote(MapInfo map) {
        int i = 1;
        for (MapInfo mapInfo : Voting.getMaps()) {
            if (mapInfo.getName().equalsIgnoreCase(map.getName())) {
                break;
            }
            i++;
        }

        scoreboard.editText("vote" + i, "§e" + Voting.getVotes(map));
    }

    public static void deleteScores(MapInfo map) {
        for (int i = 1; i < 4; i++) {
            if (scoreboard.contains("map" + i)) {
                scoreboard.deleteText("map" + i);
                scoreboard.deleteText("vote" + i);
            }
        }

        String first = map.getName();
        String second = "";
        if (map.getName().length() > 14) {
            first = map.getName().substring(0, 14);
            if (map.getName().length() >= 30) {
                second = map.getName().substring(14, 30);
            } else {
                second = map.getName().substring(14, (map.getName().length()));
            }
        }

        if (scoreboard.contains("map4")) {
            scoreboard.deleteText("map4");
            scoreboard.deleteText("vote4");
        }

        scoreboard.addText("map4", "§e" + first, "§e" + second, 14);
        scoreboard.addText("vote4", "§7Votes: ", "§e" + Voting.getVotes(map) + "", 13);
    }

    public static void update(int time) {
        scoreboard.setTitle("§bQuiz §8- §7" + Util.secToMin(time));
    }

    public static void set(Player p) {
        scoreboard.set(p);
    }
}
