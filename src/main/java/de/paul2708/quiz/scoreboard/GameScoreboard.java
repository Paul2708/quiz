package de.paul2708.quiz.scoreboard;

import de.paul2708.quiz.game.Profile;
import de.paul2708.quiz.util.Util;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.List;

/**
 * Created by Paul on 17.06.2016.
 */
public class GameScoreboard {

    private static BasicScoreboard scoreboard;

    public static boolean nextToName;

    public static void initializeScoreboard() {
        scoreboard = new BasicScoreboard("game", DisplaySlot.SIDEBAR);
        scoreboard.setTitle("§eRunde 1 §8- §7" + Util.secToMin(20));

        int score = 16;

        for (Profile profile : Profile.getProfiles()) {
            if (nextToName) {
                scoreboard.addText(profile.getName().toLowerCase(), "§7" + profile.getName(), "§e" + "  0", score);
            } else {
                scoreboard.addText(profile.getName().toLowerCase(), "§7" + profile.getName(), "   ", 0);
            }

            score--;
        }
    }

    public static void updateScores() {
        List<Profile> profiles = Util.sort(Profile.getProfiles(), true);
        int score = 16;
        int rank = 1;
        for (Profile profile : profiles) {
            profile.setRank(rank);
            profile.getPlayer().setLevel(rank);

            if (nextToName) {
                scoreboard.editText(profile.getName().toLowerCase(), "§e" + "  " + profile.getPoints(), score);
            } else {
                scoreboard.editText(profile.getName().toLowerCase(), "   ", profile.getPoints());
            }

            score--;
            rank++;
        }
    }

    public static void deletePlayer(Player p) {
        scoreboard.deleteText(p.getName().toLowerCase());
    }

    public static void update(int round, int time) {
        scoreboard.setTitle("§eRunde " + round + " §8- §7" + Util.secToMin(time));
    }

    public static void set(Player p) {
        scoreboard.set(p);
    }
}
