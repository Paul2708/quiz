package de.paul2708.quiz.file;

import de.paul2708.common.file.BasicFile;

import java.util.ArrayList;

/**
 * Created by Paul on 07.06.2016.
 */
public class MapFile extends BasicFile {

    private String mapName;

    public MapFile(String mapName) {
        super("plugins/Quiz/maps/" + mapName + ".yml");

        this.mapName = mapName;
    }

    @Override
    public void createDefaultValue() {
        comment("map settings for " + mapName);

        set("enable", false);

        // Basic
        set("world_name", "none");
        set("display_name", "none");
        set("builder", "none");
        set("icon_material", "0:0");

        // Number
        set("number.1", new ArrayList<>());
        set("number.2", new ArrayList<>());
        set("number.3", new ArrayList<>());

        // Area
        set("area.1.1", "none");
        set("area.1.2", "none");
        set("area.2.1", "none");
        set("area.2.2", "none");
        set("area.3.1", "none");
        set("area.3.2", "none");

        // Spawn
        set("spawn", "none");

        // Hologram
        set("hologram.1", "none");
        set("hologram.2", "none");
        set("hologram.3", "none");

        // Teleport
        set("teleport_height", -100);

        save();
    }

}
