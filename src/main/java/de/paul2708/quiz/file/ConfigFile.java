package de.paul2708.quiz.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 07.06.2016.
 */
public class ConfigFile extends BasicFile {

    public ConfigFile() {
        super("plugins/Quiz/configuration.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("basic quiz config");

        set("setup", true);
        set("lobby", "none");

        set("minplayers", 2);
        set("maxplayers", 10);
        set("score_next_to_name", true);
        set("host", "localhost");
        set("user", "root");
        set("password", "");

        save();
    }

    public boolean isInSetup() {
        return get("setup");
    }

    public String getLobby() {
        return get("lobby");
    }

    public int getMinPlayers() {
        return get("minplayers");
    }

    public int getMaxPlayers() {
        return get("maxplayers");
    }

    public boolean isNextToName() {
        return get("score_next_to_name");
    }

    public String getHost() {
        return get("host");
    }

    public String getUser() {
        return get("user");
    }

    public String getPassword() {
        return get("password");
    }
}
