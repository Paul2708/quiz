package de.paul2708.quiz.stats;

import de.paul2708.api.CloudAPI;
import de.paul2708.api.stats.Statistic;
import de.paul2708.api.stats.StatisticManager;
import de.paul2708.api.stats.StatisticSetup;
import de.paul2708.common.stats.Category;
import de.paul2708.quiz.util.Constants;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 25.06.2016.
 */
public class Stats {

    private static StatisticManager statisticManager;

    public static void initialize(CloudAPI api) {
        StatisticSetup setup = new StatisticSetup()
                // Name
                .name("Quiz")
                // Server tag
                .tag("Quiz")
                // Categories
                .category(new Category("Punkte", "points", Category.Type.INTEGER, 11, "0"))
                .category(new Category("Spiele", "games", Category.Type.INTEGER, 200, "0"))
                .category(new Category("Siege", "wins", Category.Type.INTEGER, 11, "0"))
                .category(new Category("Richtig", "correct_answers", Category.Type.INTEGER, 11, "0"))
                .category(new Category("Falsch", "wrong_answers", Category.Type.INTEGER, 11, "0"))
                // Command
                .command(data -> {
                    List<String> list = new ArrayList<>();
                    list.add(Constants.TAG + "§7Punkte: §e" + data.get("Punkte"));
                    list.add(Constants.TAG + "§7Gewonnene Spiele: §e" + data.get("Siege"));
                    list.add(Constants.TAG + "§7Richtige Antworten: §e" + data.get("Richtig"));
                    list.add(Constants.TAG + "§7Falsche Antworten: §e" + data.get("Falsch"));
                    list.add(Constants.TAG + "§7Gespielte Spiele: §e" + data.get("Spiele"));
                    return list;
                });

        // Register statistic manager with setup
        Stats.statisticManager = api.getStatisticManager();
        Stats.statisticManager.applySetup(setup);
        Stats.statisticManager.register();
    }

    public static Statistic getStatistic(Player player) {
        return statisticManager.getStatistic(player.getUniqueId());
    }

    public static StatisticManager getManager() {
        return statisticManager;
    }
}
