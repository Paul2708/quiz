package de.paul2708.quiz.util;

import de.paul2708.spigot.util.nms.ReflectionUtil;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian on 13.05.2017.
 */
public class PacketUtil {

    private static Constructor<?> chatComponentTextConstructor, packetPlayOutChatConstructor;
    private static Class<?> iChatBase;

    static {
        try {
            chatComponentTextConstructor = ReflectionUtil.getNMSClass("ChatComponentText").getConstructor(String.class);
            iChatBase = ReflectionUtil.getNMSClass("IChatBaseComponent");
            packetPlayOutChatConstructor = ReflectionUtil.getNMSClass("PacketPlayOutChat").getConstructor(iChatBase);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendTablist(Player p, String header, String footer) {
        if (header == null) {
            header = "";
        }
        if (footer == null) {
            footer = "";
        }

        IChatBaseComponent tabheader = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
        IChatBaseComponent tabfooter = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter(tabheader);

        try {
            Field f = packet.getClass().getDeclaredField("b");
            f.setAccessible(true);
            f.set(packet, tabfooter);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
        }
    }

    public static void setActionBar(Player p, String message) {
        try {
            Object o = chatComponentTextConstructor.newInstance(message);
            Object packet = packetPlayOutChatConstructor.newInstance(o);
            Field field = packet.getClass().getDeclaredField("b");
            field.setAccessible(true);
            field.set(packet, (byte) 2);

            ReflectionUtil.sendPacket(p, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Player> packetSend = new ArrayList<Player>();

    public static void spectate(Player p, Player target) {
        try {
            Constructor<?> packetPlayOutCameraConstructor = ReflectionUtil.getNMSClass("PacketPlayOutCamera").getConstructor();
            Object packet = packetPlayOutCameraConstructor.newInstance();
            Field field = packet.getClass().getDeclaredField("a");
            field.setAccessible(true);
            field.set(packet, target.getEntityId());

            ReflectionUtil.sendPacket(p, packet);

            packetSend.add(p);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void cancelSpectate(Player p) {
        try {
            Constructor<?> packetPlayOutCameraConstructor = ReflectionUtil.getNMSClass("PacketPlayOutCamera").getConstructor();
            Object packet = packetPlayOutCameraConstructor.newInstance();
            Field field = packet.getClass().getDeclaredField("a");
            field.setAccessible(true);
            field.set(packet, p.getEntityId());

            ReflectionUtil.sendPacket(p, packet);

            packetSend.remove(p);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle) {
        try {
            Object e;
            Object chatTitle;
            Object chatSubtitle;
            Constructor<?> subtitleConstructor;
            Object titlePacket;
            Object subtitlePacket;

            if (title != null) {
                title = ChatColor.translateAlternateColorCodes('&',title);
                title = title.replaceAll("%player%",player.getDisplayName());
                // Times packets
                e = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get((Object) null);
                chatTitle = ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke((Object) null, new Object[]{"{\"text\":\"" + title + "\"}"});
                subtitleConstructor = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getConstructor(new Class[]{ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], ReflectionUtil.getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE});
                titlePacket = subtitleConstructor.newInstance(new Object[]{e, chatTitle, fadeIn, stay, fadeOut});
                ReflectionUtil.sendPacket(player, titlePacket);

                e = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get((Object) null);
                chatTitle = ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke((Object) null, new Object[]{"{\"text\":\"" + title + "\"}"});
                subtitleConstructor = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getConstructor(new Class[]{ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], ReflectionUtil.getNMSClass("IChatBaseComponent")});
                titlePacket = subtitleConstructor.newInstance(new Object[]{e, chatTitle});
                ReflectionUtil.sendPacket(player, titlePacket);
            }

            if (subtitle != null) {
                subtitle = ChatColor.translateAlternateColorCodes('&',subtitle);
                subtitle = subtitle.replaceAll("%player%",player.getDisplayName());
                // Times packets
                e = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get((Object) null);
                chatSubtitle = ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke((Object) null, new Object[]{"{\"text\":\"" + title + "\"}"});
                subtitleConstructor = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getConstructor(new Class[]{ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], ReflectionUtil.getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE});
                subtitlePacket = subtitleConstructor.newInstance(new Object[]{e, chatSubtitle, fadeIn, stay, fadeOut});
                ReflectionUtil.sendPacket(player, subtitlePacket);

                e = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get((Object) null);
                chatSubtitle = ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke((Object) null, new Object[]{"{\"text\":\"" + subtitle + "\"}"});
                subtitleConstructor = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getConstructor(new Class[]{ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], ReflectionUtil.getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE});
                subtitlePacket = subtitleConstructor.newInstance(new Object[]{e, chatSubtitle, fadeIn, stay, fadeOut});
                ReflectionUtil.sendPacket(player, subtitlePacket);
            }
        } catch (Exception var11) {
            var11.printStackTrace();
        }
    }
}
