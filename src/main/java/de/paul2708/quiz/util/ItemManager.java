package de.paul2708.quiz.util;

import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.spigot.api.ItemBuilder;
import de.paul2708.spigot.api.PlayerAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 08.06.2016.
 */
public class ItemManager {

    public static void clearInventory(Player p) {
        p.getInventory().clear();

        p.getInventory().setChestplate(new ItemBuilder().type(Material.AIR).build());
        p.getInventory().setHelmet(new ItemBuilder().type(Material.AIR).build());
        p.getInventory().setBoots(new ItemBuilder().type(Material.AIR).build());
        p.getInventory().setLeggings(new ItemBuilder().type(Material.AIR).build());

        p.updateInventory();
    }

    public static void openForcingInventory(Player p) {
        Inventory inventory = Bukkit.createInventory(null, Util.getSize(MapInfo.getMaps().size()), "§eMap-Forcing");

        int slot = 0;
        for (MapInfo map : MapInfo.getMaps()) {
            ItemStack item = new ItemBuilder().name("§e" + map.getName()).id(map.getIconID()[0]).
                    subID(map.getIconID()[1]).description("§7von §e" + map.getBuilder()).build();
            inventory.setItem(slot, item);
            slot++;
        }

        p.openInventory(inventory);
    }

    public static void openVotingInventory(Player p) {
        Inventory inventory = Bukkit.createInventory(null, 9, "§eMap-Voting");

        int slot = 1;
        for (MapInfo map : Voting.getMaps()) {
            ItemStack item = new ItemBuilder().name("§e" + map.getName()).id(map.getIconID()[0]).
                    subID(map.getIconID()[1]).description("§7von §e" + map.getBuilder()).build();
            inventory.setItem(slot, item);
            slot += 3;
        }

        p.openInventory(inventory);
    }

    public static void giveGameItems(Player p) {
        ItemStack switcher = new ItemBuilder().name("§eSpieler-Switcher").type(Material.LEASH).
                description("§7Tausche mit einem Spieler den Platz.").build();
        ItemStack visible = new ItemBuilder().name("§eSichtbarkeits-Joker").type(Material.BLAZE_ROD).
                description("§7Schaue, wohin deine Gegenspieler gehen.").build();
        ItemStack joker = new ItemBuilder().name("§eJoker").type(Material.NETHER_STAR).
                description("§7Du bekommst weder Punkte dazu noch abgezogen").build();

        p.getInventory().setItem(0, switcher);
        p.getInventory().setItem(1, visible);
        p.getInventory().setItem(2, joker);
    }

    public static void giveVoteItems(Player p) {
        ItemStack map = new ItemBuilder().name("§eMap-Voting").type(Material.BOOK).
                description("§7Vote für deine Map").build();
        ItemStack force = new ItemBuilder().name("§eMap-Voting (Force)").type(Material.PAPER).
                description("§7Force deine Map").build();

        p.getInventory().setItem(0, map);
        if (PlayerAPI.getRank(p).getPermissionLevel() >= Permission.MAP_FORCE) {
            p.getInventory().setItem(1, force);
        }
    }

    public static void giveEditItems(Player p, String world) {
        ItemStack spawn = new ItemBuilder().name("Spawn setzen [Map=" + world + "]").description("Links- u. Rechtsklick:")
                .description("aktuelle Spielerposition").type(Material.BONE).build();

        ItemStack number = new ItemBuilder().name("Position der Lampen für Antwort 1 [Map=" + world + "]").description("Rechtsklick: geklickter Block")
                .description("Linksklick: Löschen").description("Sneake um die nächste Nummber zu setzen")
                .type(Material.BLAZE_ROD).build();

        ItemStack area = new ItemBuilder().name("Begrenzung 1 der Antwort 1 [Map=" + world + "]").description("Rechtsklick: geklickter Block")
                .description("Linksklick: Nächster Block").description("Sneake um die nächste Begrenzung zu setzen")
                .type(Material.STICK).build();

        ItemStack holo = new ItemBuilder().name("Position des Holograms für Antwort 1 [Map=" + world + "]").description("Links u. Rechtsklick:")
                .description("aktuelle Spielerposition").description("Sneake um die nächste Antwort zu setzen")
                .type(Material.RAW_FISH).build();

        p.getInventory().setItem(0, spawn);
        p.getInventory().setItem(1, number);
        p.getInventory().setItem(2, area);
        p.getInventory().setItem(3, holo);
    }
}
