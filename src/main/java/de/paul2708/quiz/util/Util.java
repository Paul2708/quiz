package de.paul2708.quiz.util;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.game.Profile;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.BlockIterator;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 07.06.2016.
 */
public class Util {

    public static Block getTargetBlock(Player player, int range) {
        BlockIterator iter = new BlockIterator(player, range);
        Block lastBlock = iter.next();
        while (iter.hasNext()) {
            lastBlock = iter.next();
            if (lastBlock.getType() == Material.AIR) {
                continue;
            }
            break;
        }
        return lastBlock;
    }

    public static void createTable() {
        String query = "CREATE TABLE IF NOT EXISTS `questions` ("
                + "`question` varchar(200) NOT NULL,"
                + "`answer_1` varchar(200) NOT NULL,"
                + "`answer_2` varchar(200) NOT NULL,"
                + "`correct_answer` varchar(200) NOT NULL,"
                + "UNIQUE KEY `question` (`question`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        Quiz.getMySQL().queryUpdate(query);
    }

    public static void spawnFirework(Location loc) {
        Firework f = loc.getWorld().spawn(loc, Firework.class);
        FireworkMeta fm = f.getFireworkMeta();
        FireworkEffect fe = FireworkEffect.builder().flicker(false).trail(true).with(FireworkEffect.Type.BALL).
                withColor(Color.GREEN).withFade(Color.GREEN).build();
        fm.addEffect(fe);
        f.setFireworkMeta(fm);
        try {
            Class<?> entityFireworkClass = getClass("net.minecraft.server.", "EntityFireworks");
            Class<?> craftFireworkClass = getClass("org.bukkit.craftbukkit.", "entity.CraftFirework");
            Object firework = craftFireworkClass.cast(f);
            Method handle = firework.getClass().getMethod("getHandle");
            Object entityFirework = handle.invoke(firework);
            Field expectedLifespan = entityFireworkClass.getDeclaredField("expectedLifespan");
            Field ticksFlown = entityFireworkClass.getDeclaredField("ticksFlown");
            ticksFlown.setAccessible(true);
            ticksFlown.setInt(entityFirework, expectedLifespan.getInt(entityFirework) - 1);
            ticksFlown.setAccessible(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static Class<?> getClass(String prefix, String nmsClassString) throws ClassNotFoundException {
        String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
        String name = prefix + version + nmsClassString;
        Class<?> nmsClass = Class.forName(name);
        return nmsClass;
    }

    public static List<Location> select(Location loc1, Location loc2, World w) {
        List<Location> list = new ArrayList<>();

        int x1 = loc1.getBlockX();
        int y1 = loc1.getBlockY();
        int z1 = loc1.getBlockZ();

        int x2 = loc2.getBlockX();
        int y2 = loc2.getBlockY();
        int z2 = loc2.getBlockZ();

        int xMin, yMin, zMin;
        int xMax, yMax, zMax;
        int x, y, z;

        if (x1 > x2) {
            xMin = x2;
            xMax = x1;
        } else {
            xMin = x1;
            xMax = x2;
        }

        if (y1 > y2) {
            yMin = y2;
            yMax = y1;
        } else {
            yMin = y1;
            yMax = y2;
        }

        if (z1 > z2) {
            zMin = z2;
            zMax = z1;
        } else {
            zMin = z1;
            zMax = z2;
        }

        for (x = xMin; x <= xMax; x++) {
            for (y = yMin; y <= yMax; y++) {
                for (z = zMin; z <= zMax; z++) {
                    list.add(new Location(w, x, y, z));
                }
            }
        }

        return list;
    }

    public static void sendActionBar(String msg) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            // IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + msg + "\"}");
            // PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
            // ((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
            IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" +
                    ChatColor.translateAlternateColorCodes('&', msg) + "\"}");
            PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte)2);
            ((CraftPlayer)p).getHandle().playerConnection.sendPacket(bar);
        }
    }

    public static void clearChat() {
        for (int i = 0; i < 100; i++) {
            Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(" "));
        }
    }

    private static List<Profile> temp = new ArrayList<>();

    public static List<Profile> sort(List<Profile> list, boolean reset) {
        if (reset) Util.temp.clear();
        if (list.size() == 0) return Util.temp;

        List<Profile> temp = list;
        int max = -100;
        Profile best = null;
        for (Profile profile : list) {
            if (Math.max(max, profile.getPoints()) == profile.getPoints()) {
                max = profile.getPoints();
                best = profile;
            }
        }

        Util.temp.add(best);
        temp.remove(best);
        sort(temp, false);
        return Util.temp;
    }

    public static int getSize(int count) {
        if(count <= 8) return 9;
        if(count <= 17) return 18;
        if(count <= 26) return 27;
        if(count <= 35) return 36;
        if(count <= 44) return 45;
        return -1;
    }

    public static void prepareWorld(String name) {
        World w = Bukkit.createWorld(new WorldCreator(name));
        w.setAmbientSpawnLimit(0);
        w.setDifficulty(Difficulty.NORMAL);
        w.setMonsterSpawnLimit(0);
        w.setPVP(true);
        w.setGameRuleValue("doDaylightCycle", "false");
        w.setStorm(false);
        w.setThundering(false);
        w.setTime(6000);
        w.setAutoSave(false);

        w.getEntities().stream().filter(all -> !(all instanceof Player) && !(all instanceof Villager)).forEach(Entity::remove);
    }

    public static String secToMin(int i) {
        int ms = i / 60;
        int ss = i % 60;
        String m = (ms < 10 ? "0" : "") + ms;
        String s = (ss < 10 ? "0" : "") + ss;
        String f = m + ":" + s;
        return f;
    }

    public static void preparePlayer(Player p) {
        p.setHealth(20.0);
        p.setMaxHealth(20.0);
        p.setFoodLevel(20);
        p.setFireTicks(0);

        ItemManager.clearInventory(p);

        p.setAllowFlight(false);
        p.setFlying(false);

        for (PotionEffect all : p.getActivePotionEffects()) {
            p.removePotionEffect(all.getType());
        }

        p.setExp(0);
        p.setLevel(0);

        p.setGameMode(GameMode.SURVIVAL);
    }

    public static List<String> getAllFiles() {
        List<String> fileNames = new ArrayList<String>();
        File folder = new File("plugins/Quiz/maps");
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if(file.isFile()) {
                fileNames.add(file.getName().split("\\.")[0]);
            }
        }

        return fileNames;
    }
}
