package de.paul2708.quiz.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Christian on 13.05.2017.
 */
public class MessageUtil {

    public static void sendMessageToAll(String message) {
        for(Player all : Bukkit.getOnlinePlayers()) {
            all.sendMessage(message);
        }
    }
}
