package de.paul2708.quiz.util;

import de.paul2708.quiz.Quiz;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;

/**
 * Created by Paul on 07.06.2016.
 */
public class LocationUtil {

    public static String getLocationAsString(Location loc) {
        String world = loc.getWorld().getName();
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        float yaw = loc.getYaw();
        float pitch = loc.getPitch();
        String location = world + ":" + x + ":" + y + ":" + z + ":" + yaw + ":" + pitch;
        return location;
    }

    public static Location getLocationFromString(String s) {
        String[] array = s.split(":");
        final String world = array[0];
        double x = Double.valueOf(array[1]);
        double y = Double.valueOf(array[2]);
        double z = Double.valueOf(array[3]);
        float yaw = Float.valueOf(array[4]);
        float pitch = Float.valueOf(array[5]);

        Bukkit.getScheduler().scheduleSyncDelayedTask(Quiz.getInstance(), () -> Bukkit.createWorld(new WorldCreator(world)));

        Location loc = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        return loc;
    }
}
