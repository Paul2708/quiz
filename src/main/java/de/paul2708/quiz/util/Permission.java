package de.paul2708.quiz.util;

/**
 * Created by Paul on 11.06.2016.
 */
public class Permission {

    public static final int MAP_FORCE = 80;
    public static final int START = 50;
    public static final int ALL_COMMANDS = 50;
}
