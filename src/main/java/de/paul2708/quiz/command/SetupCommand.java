package de.paul2708.quiz.command;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 08.06.2016.
 */
public class SetupCommand extends BasicCommand {

    public SetupCommand() {
        super(Quiz.getInstance(), "setup", 120, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (Quiz.getConfigFile().isInSetup()) {
            Quiz.getConfigFile().set("setup", false);
            p.sendMessage(Constants.TAG + "§7Restarte den Server um die Konfiguration neu zu laden.");
        } else {
            ServerAPI.setGameState(GameState.SETUP);

            Quiz.getConfigFile().set("setup", true);
            p.sendMessage(Constants.TAG + "§7Der Server befindet sich nun im Setup.");
            p.sendMessage(Constants.TAG + "§7Weitere Commands aktiv: §eaddmap, deletemap, editmap, maplist");
        }

        Quiz.getConfigFile().save();
    }
}
