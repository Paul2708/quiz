package de.paul2708.quiz.command;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.ItemManager;
import de.paul2708.spigot.api.BasicCommand;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 08.06.2016.
 */
public class EditMapCommand extends BasicCommand {

    public EditMapCommand() {
        super(Quiz.getInstance(), "editmap", 120, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (!Quiz.getConfigFile().isInSetup()) {
            p.sendMessage(Constants.TAG + "§7Du musst den Setup-Mode aktivieren um den Befehl ausführen zu können.");
        } else {
            if (args.length != 1) {
                p.sendMessage(Constants.TAG + "§7Nutze §e/editmap [Weltname]");
            } else {
                String world = args[0];

                if (!MapInfo.doesMapExist(world)) {
                    p.sendMessage(Constants.TAG + "§7Die Map §e" + world + " §7existiert nicht.");
                } else {
                    ItemManager.clearInventory(p);
                    ItemManager.giveEditItems(p, world);
                    p.sendMessage(Constants.TAG + "§7Konfiguriere nun die Map. Anleitung auf den Items.");
                }
            }
        }
    }
}
