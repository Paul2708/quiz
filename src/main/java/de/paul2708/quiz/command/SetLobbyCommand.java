package de.paul2708.quiz.command;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.LocationUtil;
import de.paul2708.spigot.api.BasicCommand;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 11.06.2016.
 */
public class SetLobbyCommand extends BasicCommand {

    public SetLobbyCommand() {
        super(Quiz.getInstance(), "setlobby", 120, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (!Quiz.getConfigFile().isInSetup()) {
            p.sendMessage(Constants.TAG + "§7Du musst den Setup-Mode aktivieren um den Befehl ausführen zu können.");
        } else {
            Quiz.getConfigFile().set("lobby", LocationUtil.getLocationAsString(p.getLocation()));
            Quiz.getConfigFile().save();
            p.sendMessage(Constants.TAG + "§7Du hast den §eLobby-Spawn §7gesetzt.");
        }
    }
}
