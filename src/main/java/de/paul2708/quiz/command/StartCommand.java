package de.paul2708.quiz.command;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.util.Constants;
import de.paul2708.quiz.util.Permission;
import de.paul2708.spigot.api.BasicCommand;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 17.06.2016.
 */
public class StartCommand extends BasicCommand {

    public StartCommand() {
        super(Quiz.getInstance(), "start", Permission.START, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (ServerAPI.getState() != GameState.LOBBY) {
            p.sendMessage(Constants.TAG + "§cDie Runde hat bereits gestartet.");
            return;
        }
        if (!Game.getLobbyCountdown().isRunning()) {
            p.sendMessage(Constants.TAG + "§cDa die Runde noch nicht voll ist, kannst du nicht starten.");
            return;
        }
        if (Game.getLobbyCountdown().getRemainingTime() <= 10) {
            p.sendMessage(Constants.TAG + "§cDie Runde startet bereits.");
            return;
        }
        if (args.length > 1) {
            p.sendMessage(Constants.TAG + "§aNutze /start");
        } else if (args.length == 0){
            Game.getLobbyCountdown().setTime(6);
            p.sendMessage(Constants.TAG + "§aSpiel wird gestartet...");
            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
        } else {
            int time;
            try {
                time = Integer.valueOf(args[0]);
            } catch(NumberFormatException e) {
                p.sendMessage(Constants.TAG + "§cDie Angabe " + args[0] + " ist nicht gültig.");
                return;
            }
            if(time <= 4) {
                p.sendMessage(Constants.TAG + "§cSetzte die Zeit auf minimal 5 Sekunden.");
                return;
            }

            Game.getLobbyCountdown().setTime(time + 1);
            p.sendMessage(Constants.TAG + "§aDer Countdown wurde auf §c" + time + " §aSekunden verkürzt.");
        }
    }
}
