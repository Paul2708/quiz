package de.paul2708.quiz.command;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.BasicCommand;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 07.06.2016.
 */
public class MapListCommand extends BasicCommand {

    public MapListCommand() {
        super(Quiz.getInstance(), "maplist", 120, null);
    }

    @Override
    public void run(Player p, String[] args) {
        p.sendMessage(Constants.TAG + "§7Folgende Maps wurden gefunden:");
        for (MapInfo info : MapInfo.getMaps()) {
            p.sendMessage("Map: §e" + info.getName() + " §7Aktiviert: §e" +
                    (info.isEnabled() ? "ja" : "nein"));
        }
    }
}
