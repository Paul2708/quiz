package de.paul2708.quiz.command;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.file.MapFile;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.BasicCommand;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 07.06.2016.
 */
public class AddMapCommand extends BasicCommand {

    public AddMapCommand() {
        super(Quiz.getInstance(), "addmap", 120, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (!Quiz.getConfigFile().isInSetup()) {
            p.sendMessage(Constants.TAG + "§7Du musst den Setup-Mode aktivieren um den Befehl ausführen zu können.");
        } else {
            if (args.length != 4) {
                p.sendMessage(Constants.TAG + "§7Nutze §e/addmap [world_name] [name] [builder] [icon]");
                p.sendMessage(Constants.TAG + "§7Bsp.: /addmap test_world Test Paul2708 33:1");
            } else {
                String worldName = args[0];
                String name = args[1];
                String builder = args[2];
                String icon = args[3];

                try {
                    if(icon.contains(":")) {
                        Integer.valueOf(icon.split(":")[0]);
                        Integer.valueOf(icon.split(":")[1]);
                    } else {
                        Integer.valueOf(icon);
                    }

                } catch(Exception e) {
                    p.sendMessage(Constants.TAG + "§7Die ID wurde nicht richtig angegeben. (" + icon + ")");
                    return;
                }

                if (MapInfo.doesMapExist(worldName)) {
                    p.sendMessage(Constants.TAG + "§7Die Map §e'" + worldName + "' §7wurde erfolgreich überschrieben.");
                } else {
                    p.sendMessage(Constants.TAG + "§7Die Map §e'" + worldName + "' §7wurde erfolgreich erstellt.");
                }

                MapFile file = MapInfo.getInfo(worldName).getFile();

                file.set("world_name", worldName);
                file.set("display_name", name);
                file.set("builder", builder);
                file.set("icon_material", icon);

                file.save();
            }
        }
    }

}
