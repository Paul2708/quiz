package de.paul2708.quiz.command;

import de.paul2708.quiz.Quiz;
import de.paul2708.quiz.file.MapFile;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.util.Constants;
import de.paul2708.spigot.api.BasicCommand;
import org.bukkit.entity.Player;

/**
 * Created by Paul on 07.06.2016.
 */
public class DeleteMapCommand extends BasicCommand {

    public DeleteMapCommand() {
        super(Quiz.getInstance(), "deletemap", 120, null);
    }

    @Override
    public void run(Player p, String[] args) {
        if (!Quiz.getConfigFile().isInSetup()) {
            p.sendMessage(Constants.TAG + "§7Du musst den Setup-Mode aktivieren um den Befehl ausführen zu können.");
        } else {
            if(args.length != 1) {
                p.sendMessage(Constants.TAG + "§7Nutze §e/deletemap [world_name]");
            } else {
                if(!MapInfo.doesMapExist(args[0])) {
                    p.sendMessage(Constants.TAG + "§7Die Map §e'" + args[0] + "' §7gibt es nicht.");
                } else {
                    p.sendMessage(Constants.TAG + "§7Die Map §e'" + args[0] + "' §7wurde erfolgreich gelöscht.");
                }

                MapFile file = MapInfo.getInfo(args[0]).getFile();
                file.delete();
                MapInfo.cache.remove(args[0]);
            }
        }
    }
}
