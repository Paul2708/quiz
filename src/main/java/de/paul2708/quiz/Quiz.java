package de.paul2708.quiz;

import de.paul2708.common.game.GameState;
import de.paul2708.quiz.command.*;
import de.paul2708.quiz.file.ConfigFile;
import de.paul2708.quiz.game.Game;
import de.paul2708.quiz.game.Voting;
import de.paul2708.quiz.listener.*;
import de.paul2708.quiz.listener.game.GameInteractEntityListener;
import de.paul2708.quiz.listener.game.GameInteractListener;
import de.paul2708.quiz.listener.lobby.VotingInteractListener;
import de.paul2708.quiz.listener.lobby.VotingInventoryListener;
import de.paul2708.quiz.listener.setup.SetupBlockBreakListener;
import de.paul2708.quiz.listener.setup.SetupPlayerInteractListener;
import de.paul2708.quiz.listener.setup.SetupPlayerToggleSneakListener;
import de.paul2708.quiz.map.MapInfo;
import de.paul2708.quiz.scoreboard.LobbyScoreboard;
import de.paul2708.quiz.stats.Stats;
import de.paul2708.quiz.util.Util;
import de.paul2708.spigot.CloudSpigot;
import de.paul2708.spigot.api.MySQL;
import de.paul2708.spigot.api.ServerAPI;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by Paul on 07.06.2016.
 */
public class Quiz extends JavaPlugin {

    @Override
    public void onLoad() {
        Quiz.instance = this;
    }

    @Override
    public void onEnable() {

        System.out.println(CloudSpigot.getInstance());

        loadFiles();

        Quiz.mySQL = new MySQL(config.getHost(), "Quiz", config.getUser(), config.getPassword());
        Quiz.mySQL.openConnection();
        Util.createTable();

        Stats.initialize(CloudSpigot.getCloudAPI());
        Game.initialize();

        if (getConfigFile().isInSetup()) {
            ServerAPI.setGameState(GameState.SETUP);
        } else {
            ServerAPI.setGameState(GameState.LOBBY);

            Voting.selectMaps();
            LobbyScoreboard.initializeScoreboard();
        }

        registerListener();
        registerCommands();

        ServerAPI.setMotd("Voting");
    }

    @Override
    public void onDisable() {

    }

    private static Quiz instance;
    private static ConfigFile config;
    private static MySQL mySQL;

    public static Quiz getInstance() {
        return instance;
    }

    public static ConfigFile getConfigFile() {
        return config;
    }

    public static MySQL getMySQL() {
        return mySQL;
    }

    private void registerCommands() {
        new AddMapCommand();
        new DeleteMapCommand();
        new MapListCommand();
        new SetupCommand();
        new EditMapCommand();
        new SetLobbyCommand();
        new StartCommand();
    }

    private void registerListener() {
        PluginManager pm = getServer().getPluginManager();

        // General
        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new CancelListener(), this);
        pm.registerEvents(new PlayerQuitListener(), this);
        pm.registerEvents(new AsyncPlayerChatListener(), this);
        pm.registerEvents(new PlayerMoveListener(), this);
        pm.registerEvents(new PlayerCommandPreprocessListener(), this);
        // Setup
        pm.registerEvents(new SetupPlayerInteractListener(), this);
        pm.registerEvents(new SetupBlockBreakListener(), this);
        pm.registerEvents(new SetupPlayerToggleSneakListener(), this);
        // Lobby
        pm.registerEvents(new VotingInteractListener(), this);
        pm.registerEvents(new VotingInventoryListener(), this);
        // Game
        pm.registerEvents(new GameInteractListener(), this);
        pm.registerEvents(new GameInteractEntityListener(), this);
        // API
        pm.registerEvents(new PlayerLoadedListener(), this);
    }

    private void loadFiles() {
        Quiz.config = new ConfigFile();

        new File("plugins/Quiz/maps").mkdirs();
        Util.getAllFiles().forEach(MapInfo::new);
    }
}
